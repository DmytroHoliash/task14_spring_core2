package com.holiash;

import com.holiash.bean1.BeanA;
import com.holiash.bean1.BeanB;
import com.holiash.carbeans.CarCollection;
import com.holiash.configs.BeanProfileConfig;
import com.holiash.configs.CarConfig;
import com.holiash.configs.ComponentProfileConfig;
import com.holiash.configs.Config1;
import com.holiash.configs.Config2;
import com.holiash.otherbeans.OtherBeanA;
import com.holiash.otherbeans.OtherBeanB;
import com.holiash.profilemodel.BeanData;
import com.holiash.profilemodel.MyComponentData;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

public class Application {

  public static void main(String[] args) {
    ApplicationContext context = new AnnotationConfigApplicationContext(Config1.class,
      Config2.class, CarConfig.class);
    for (String beanName : context.getBeanDefinitionNames()) {
      System.out.println(context.getBean(beanName).getClass().toString());
    }
    System.out.println();
    context.getBean(CarCollection.class).show();
    OtherBeanA otherBeanA1 = context.getBean(BeanA.class).otherBeanA;
    OtherBeanA otherBeanA2 = context.getBean(OtherBeanA.class);
    System.out.println(otherBeanA1 + "    " + otherBeanA2);
    OtherBeanB otherBeanB1 = context.getBean(BeanB.class).otherBeanB;
    OtherBeanB otherBeanB2 = context.getBean(OtherBeanB.class);
    System.out.println(otherBeanB1 + "    " + otherBeanB2);
    System.out.println();
    AnnotationConfigApplicationContext context1 = new
      AnnotationConfigApplicationContext();
    context1.getEnvironment().setActiveProfiles("dev");
    context1.register(BeanProfileConfig.class);
    context1.refresh();
    System.out.println(context1.getBean(BeanData.class));
    System.out.println();
    AnnotationConfigApplicationContext context2 = new
      AnnotationConfigApplicationContext();
    context2.getEnvironment().setActiveProfiles("prod");
    context2.register(BeanProfileConfig.class);
    context2.refresh();
    System.out.println(context2.getBean(BeanData.class));

    AnnotationConfigApplicationContext context3 = new
      AnnotationConfigApplicationContext();
    context3.getEnvironment().setActiveProfiles("dev");
    context3.register(ComponentProfileConfig.class);
    context3.refresh();
    System.out.println(context3.getBean(MyComponentData.class));

    AnnotationConfigApplicationContext context4 = new
      AnnotationConfigApplicationContext();
    context4.getEnvironment().setActiveProfiles("prod");
    context4.register(ComponentProfileConfig.class);
    context4.refresh();
    System.out.println(context4.getBean(MyComponentData.class));
  }

}
