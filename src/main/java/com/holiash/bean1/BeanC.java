package com.holiash.bean1;

import com.holiash.otherbeans.OtherBeanC;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class BeanC {
  public OtherBeanC otherBeanC;

  @Autowired
  public void setOtherBeanC(OtherBeanC otherBeanC) {
    this.otherBeanC = otherBeanC;
  }
}
