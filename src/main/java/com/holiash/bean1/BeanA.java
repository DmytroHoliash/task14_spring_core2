package com.holiash.bean1;

import com.holiash.otherbeans.OtherBeanA;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class BeanA {
  public OtherBeanA otherBeanA;

  @Autowired
  public BeanA(OtherBeanA otherBeanA) {
    this.otherBeanA = otherBeanA;
  }
}
