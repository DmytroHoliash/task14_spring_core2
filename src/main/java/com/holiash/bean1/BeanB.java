package com.holiash.bean1;

import com.holiash.otherbeans.OtherBeanB;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class BeanB {
  @Autowired
  public OtherBeanB otherBeanB;
}
