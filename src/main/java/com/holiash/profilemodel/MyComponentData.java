package com.holiash.profilemodel;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class MyComponentData {
  private ComponentData componentData;

  @Autowired
  public MyComponentData(ComponentData componentData) {
    this.componentData = componentData;
  }

  @Override
  public String toString() {
    return componentData.toString();
  }
}
