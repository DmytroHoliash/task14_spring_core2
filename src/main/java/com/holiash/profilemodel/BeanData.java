package com.holiash.profilemodel;

public class BeanData {
  private String name;
  private int id;

  public BeanData(String name, int id) {
    this.name = name;
    this.id = id;
  }

  @Override
  public String toString() {
    return "BeanData{" +
      "name='" + name + '\'' +
      ", id=" + id +
      '}';
  }
}
