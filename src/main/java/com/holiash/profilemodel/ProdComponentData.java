package com.holiash.profilemodel;

import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Component;

@Component
@Profile("prod")
public class ProdComponentData extends ComponentData {
  public ProdComponentData() {
    name = "prod";
    id = 2;
  }
  public String toString() {
    return "name=" + name + "\nid=" + id;
  }
}
