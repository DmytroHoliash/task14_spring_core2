package com.holiash.profilemodel;

import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Component;

@Component
@Profile("dev")
public class DevelopmentComponentData extends ComponentData {
  public DevelopmentComponentData() {
    name = "dev";
    id = 1;
  }
  public String toString() {
    return "name=" + name + "\nid=" + id;
  }
}
