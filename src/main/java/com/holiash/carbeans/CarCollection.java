package com.holiash.carbeans;

import com.holiash.Car;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class CarCollection {
  @Autowired
  private List<Car> cars;

  public void show() {
    cars.forEach(car -> System.out.println(car.getCar()));
  }
}
