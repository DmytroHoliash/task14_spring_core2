package com.holiash.carbeans;

import com.holiash.Car;
import org.springframework.context.annotation.Primary;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;

@Component
@Order(1)
@Primary
public class Zaz implements Car {

  @Override
  public String getCar() {
    return "ZAZ Zaporozhets";
  }
}
