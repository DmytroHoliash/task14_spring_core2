package com.holiash.carbeans;

import com.holiash.Car;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;

@Component
public class CarBoss {
  @Autowired
  Car bean1;

  @Autowired
  @Qualifier("Bmw")
  Car bean2;

  @Autowired
  @Qualifier("Lada")
  Car bean3;

  @Autowired
  @Qualifier("Volynanka")
  Car bean4;
}
