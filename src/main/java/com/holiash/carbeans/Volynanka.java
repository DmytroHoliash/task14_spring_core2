package com.holiash.carbeans;

import com.holiash.Car;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;

@Component
@Qualifier("Volynanka")
@Order(2)
public class Volynanka implements Car {

  @Override
  public String getCar() {
    return "Volynanka";
  }
}
