package com.holiash.carbeans;

import com.holiash.Car;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;

@Component
@Order(3)
@Qualifier("Lada")
public class Lada implements Car {

  @Override
  public String getCar() {
    return "Lada kalina";
  }
}
