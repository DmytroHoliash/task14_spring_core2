package com.holiash.configs;

import com.holiash.profilemodel.BeanData;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.ComponentScan.Filter;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.FilterType;

@Configuration
@ComponentScan(basePackages = "com.holiash.profilemodel",
  excludeFilters = @Filter(type = FilterType.ASSIGNABLE_TYPE, classes = BeanData.class)
)
public class ComponentProfileConfig {

}
