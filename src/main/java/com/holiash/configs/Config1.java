package com.holiash.configs;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

@Configuration
@ComponentScan(basePackages = "com.holiash.bean1")
@ComponentScan(basePackages = "com.holiash.otherbeans")
public class Config1 {

}
