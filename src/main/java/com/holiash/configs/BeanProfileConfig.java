package com.holiash.configs;

import com.holiash.profilemodel.BeanData;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;

@Configuration
@ComponentScan
public class BeanProfileConfig {
  @Bean
  @Profile("dev")
  public BeanData getBeanDataForDevelopment() {
    String name = "dev profile";
    int id = 1;
    return new BeanData(name, id);
  }

  @Bean
  @Profile("prod")
  public BeanData getBeanDataForProduction() {
    String name = "prod profile";
    int id = 2;
    return new BeanData(name, id);
  }
}
