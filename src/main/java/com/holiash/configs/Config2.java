package com.holiash.configs;

import com.holiash.bean3.BeanE;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.ComponentScan.Filter;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.FilterType;

@Configuration
@ComponentScan( basePackages = "com.holiash.bean2", useDefaultFilters = false,
  includeFilters = @Filter(type = FilterType.REGEX, pattern = ".*Flower")
)
@ComponentScan(basePackages = "com.holiash.bean3",
  excludeFilters = @Filter(type = FilterType.ASSIGNABLE_TYPE, classes = BeanE.class)
)
public class Config2 {

}
